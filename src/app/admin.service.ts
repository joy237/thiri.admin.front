import { Injectable } from '@angular/core';
import { HttpClient,HttpHeaders } from '@angular/common/http';
import { Observable, throwError, of } from 'rxjs';
import { retry, catchError } from 'rxjs/operators';
import {environment} from '../environments/environment';

@Injectable({
  providedIn: 'root'
})
export class AdminService {

  constructor(private http : HttpClient) { }

    api=environment.apiUrl;
    httpOptions ={
      headers:new HttpHeaders({
        'content-type' : 'multipart/form-data'
      })
    }


   // Adminlogin
    postLogin(email,password):Observable<any>{
      return this.http.post<any>(`${this.api}/admin/login`,{
        email:email,
        password:password
      }).pipe(retry(1),catchError(val=>of(`I caught: ${val}`)))
    }

    
   //createEvent
   postEvent(name,desc,file):Observable<any>{
     console.log(file);
     const fileData = new FormData()
     fileData.append('eventimg',file);
     fileData.append('name',name);
     fileData.append('desc',desc);
     return this.http.post<any>(`${this.api}/event/createEvent`,{
       name:name,
       desc:desc,
       eventimg:file,
       headers:Headers
            }).pipe(retry(1),catchError(val=>of(`I caught: ${val}`)))
   }

   getAllEvent(){
     return this.http.post<any>(`${this.api}/event/getAllEvent`,{

     }).pipe(retry(1),catchError(val=>of(`I caught: ${val}`)))
   }


}
