import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';


import { AppRoutingModule } from './app-routing.module';
import { AppComponent } from './app.component';
import { LoginComponent } from './login/login.component';

import { HttpClientModule } from '@angular/common/http';
import {FormsModule} from "@angular/forms";


import { Routes,RouterModule } from '@angular/router';
import {BrowserAnimationsModule} from '@angular/platform-browser/animations';
import {MatButtonModule,MatIconModule,MatInputModule,MatSelectModule, MatFormFieldModule,MatSliderModule,MatToolbarModule,MatCardModule,MatSlideToggleModule} from '@angular/material';
import 'hammerjs';
import { DashboardComponent } from './dashboard/dashboard.component';
import { NavbarComponent } from './navbar/navbar.component';
import { ServicesComponent } from './services/services.component';
import { EventComponent } from './event/event.component';
import { EventEditComponent } from './event-edit/event-edit.component';
import { RegisterComponent } from './register/register.component';

const routes : Routes =[
  {path:'',component:LoginComponent},
  {path:'dashboard',component:DashboardComponent},
  {path:'services',component:ServicesComponent},
  {path:'events',component:EventComponent},
  {path:'edit-event',component:EventEditComponent},
 
  
]

@NgModule({
  declarations: [
    AppComponent,
    LoginComponent,
    DashboardComponent,
    NavbarComponent,
    ServicesComponent,
    EventComponent,
    EventEditComponent,
    RegisterComponent
  ],
  imports: [
    RouterModule.forRoot(routes),
    FormsModule,
    HttpClientModule,
    BrowserAnimationsModule,
    MatButtonModule,MatIconModule,MatInputModule,MatSelectModule,MatSliderModule, MatFormFieldModule,
    MatToolbarModule,MatCardModule,MatSlideToggleModule,
    BrowserModule,
    AppRoutingModule
  ],
  providers: [],
  bootstrap: [AppComponent]
})
export class AppModule { }
