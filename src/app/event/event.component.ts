import { Component, OnInit } from '@angular/core';
import {AdminService} from '../admin.service';
import { ActivatedRoute, Router } from '@angular/router';

@Component({
  selector: 'app-event',
  templateUrl: './event.component.html',
  styleUrls: ['./event.component.css']
})
export class EventComponent implements OnInit {

  constructor(private adminservice:AdminService) { }


  name:string;
  desc:string;
  eventimg;


  ngOnInit() {
  }


  onFileChanged(event){
    this.eventimg= event.target.files[0];
    var reader = new FileReader();
    // reader.readAsDataURL(event.target.files[0]);

    // reader.onload = (event) =>{
    //   this.eventimg=event.target.result;
    // }
    
    console.log(this.name);

  }

  uploadData(){
this.adminservice.postEvent(this.name,this.desc,this.eventimg).subscribe(res=>{
  console.table(res);
})
  }

}
