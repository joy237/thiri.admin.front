import { Component, OnInit } from '@angular/core';
import {AdminService} from '../admin.service';
import { ActivatedRoute, Router } from '@angular/router';


@Component({
  selector: 'app-event-edit',
  templateUrl: './event-edit.component.html',
  styleUrls: ['./event-edit.component.css']
})
export class EventEditComponent implements OnInit {

  constructor(private adminservice:AdminService) { }

  events:any[];

  ngOnInit() {
    this.adminservice.getAllEvent().subscribe(res=>{
     this.events = res.event;
     console.table(this.events);
    })
  }

}
