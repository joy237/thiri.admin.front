import { Component, OnInit } from '@angular/core';
import {AdminService} from '../admin.service';
import { ActivatedRoute, Router } from '@angular/router';

@Component({
  selector: 'app-login',
  templateUrl: './login.component.html',
  styleUrls: ['./login.component.css']
})
export class LoginComponent implements OnInit {

  constructor(private route: ActivatedRoute, private router: Router ,private adminservice :AdminService) { }

  ngOnInit() {
  }

  email: string;
  password: string;


  login() {
    this.adminservice.postLogin(this.email,this.password).subscribe(res =>{
      console.table(res.status);
      if( res.status == '200'){
        this.router.navigate(["/dashboard"]);

      }else{
        alert("Invalid Credentials");
      }
    })
  } 


  newAdmin(){
    
  }

}
